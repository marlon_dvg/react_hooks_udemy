import React, { useState } from 'react';
import { Container, Typography, Card, Grid, TextField, Button, Icon } from '@material-ui/core';
import styles from './style';
import { MovieIcon } from '../../icons';
import { Search } from '@material-ui/icons';

export default () => {
    const [searchText, setSearchText] = useState('');
    const classes = styles();

    const handleSearchTextChange = event => {
        setSearchText(event.target.value)
    };

    const handleCleanTextClick = event => {
        console.log(5)
    }

    const handleSearchTextClick = event => {
        console.log(15)
    }

    return (
        <Container className={classes.container} >
            <Card className={classes.cardContainer}>
                <Grid container  className={classes.titleGridContainer}>
                    <Grid>
                        <Typography  className={classes.title}>¡Bienvenido!</Typography>
                    </Grid>
                    <Grid>
                        <MovieIcon className={classes.movieIcon}/>                        
                    </Grid>
                </Grid>
                <TextField
                    className={classes.textFieldSearch}
                    value={searchText}
                    label="Buscar"
                    variant="outlined"
                    onChange={handleSearchTextChange} />
                <Grid className={classes.buttonsContainer}> 
                    <Button                         
                        color="secondary"
                        onClick={handleCleanTextClick} >
                        Limpiar
                    </Button>
                    <Button 
                        className={classes.searchButton} 
                        variant="contained" 
                        color="primary"
                        onClick={handleSearchTextClick}
                        startIcon={<Search/>} >
                        Buscar
                    </Button>
                </Grid>
            </Card>
        </Container>
    );
}
